# Copyright 2021
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from infotrans.app_window import InfotransWindow
from infotrans.confman import ConfMan
from infotrans.setup_window import SetupWindow
from gi.repository import Gtk, Gio, Handy, Gdk
import sys

import gi

gi.require_version('Gtk', '3.0')
gi.require_version('Handy', '1')


class Application(Gtk.Application):
    def __init__(self):
        super().__init__(application_id='lt.alm.stalas.Infotrans',
                         flags=Gio.ApplicationFlags.FLAGS_NONE)
        self.confman = ConfMan("infotransrc")

    def do_activate(self):
        self._init_css()

        if self.confman.get_value("setup_completed"):
            self.launch_main()
        else:
            self.launch_setup()

    def launch_setup(self):
        """
        Launch the setup of Infotrans

        This is launched if the setup_completed value in the configuration
        isn't set, this is designed to extract the username and password
        of the user.

        After completion, they will be immediately put into the main window
        """
        self.setup_win = self.props.active_window

        if not self.setup_win:
            self.setup_win = SetupWindow(
                application=self, confman=self.confman)
            # Because I couldn't figure out how to close and then launch from a thread
            # This isn't perfect however since it may have been destroyed by the user,
            # so in it there is a check for setup_completed
            self.setup_win.connect("destroy", self.launch_main)
            self.add_window(self.setup_win)
            self.setup_win.present()

    def launch_main(self, *args):
        """
        Launch the main application window

        Designed to be called either when launching and setup is completed,
        or at the end of the setup
        """
        # Because this might have been called by the window closing by the user, not because
        # the setup has been complete, we also need to check if the setup has been completed
        # here
        if self.confman.get_value("setup_completed"):
            self.main_win = InfotransWindow(
                application=self, confman=self.confman)
            self.add_window(self.main_win)
            self.main_win.present()
        else:
            self.launch_setup()
            return

    def do_startup(self):
        Gtk.Application.do_startup(self)
        Handy.init()

    def _init_css(self):
        """
        Initialize the CSS of the application
        """
        stylecontext = Gtk.StyleContext()
        provider = Gtk.CssProvider()
        provider.load_from_resource(
            f'lt/alm/stalas/Infotrans/ui/gtk_style.css'
        )
        stylecontext.add_provider_for_screen(
            Gdk.Screen.get_default(),
            provider,
            Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION
        )


def main(version):
    app = Application()
    return app.run(sys.argv)
