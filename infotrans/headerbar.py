# Copyright 2021
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>

from gi.repository import Gtk, GObject, Handy
from infotrans.main_stack import MainStack


class InfotransHeaderbar(Handy.WindowHandle):
    """
    The Infotrans main headerbar

    The headerbar of the a app, mainly containing the view switcher,
    It has been mostly copied from What IP by GabMus (should probably
    be acknowledged somewhere)
    """

    __gsignals__ = {
        'headerbar_squeeze': (
            GObject.SignalFlags.RUN_FIRST,
            None,
            (bool,)
        )
    }

    def __init__(self, main_stack: MainStack, **kwargs):
        """
        Create the Infotrans headerbar

        parameters:
            :main_stack: - the main stack of the application, it
            contains all the application pages.
        """
        super().__init__(**kwargs)
        self._main_stack = main_stack
        self._builder = Gtk.Builder.new_from_resource(
            '/lt/alm/stalas/Infotrans/ui/headerbar.ui'
        )
        self._builder.connect_signals(self)

        self.headerbar = self._builder.get_object('headerbar')
        self._squeezer = Handy.Squeezer(orientation=Gtk.Orientation.HORIZONTAL)
        self._squeezer.set_homogeneous(False)
        self._squeezer.set_interpolate_size(False)
        self._squeezer.set_hexpand(False)

        self.nobox = Gtk.Label()

        self._view_switcher = Handy.ViewSwitcher()
        self._view_switcher.set_stack(self._main_stack)
        self._view_switcher.set_margin_start(12)
        self._view_switcher.set_margin_end(12)
        self._view_switcher.set_policy(Handy.ViewSwitcherPolicy.WIDE)
        self._squeezer.add(self._view_switcher)
        self._squeezer.add(self.nobox)
        self._squeezer.connect('notify::visible-child', self._on_squeeze)
        self.headerbar.set_custom_title(self._squeezer)

        self.add(self.headerbar)
        self._menu_btn = self._builder.get_object(
            'menu_btn'
        )
        self._menu_popover = Gtk.PopoverMenu()
        self._menu_btn.set_popover(self._menu_popover)

        self._set_headerbar_controls()

    def _on_squeeze(self, *args):
        self.emit(
            'headerbar_squeeze',
            self._squeezer.get_visible_child() == self.nobox
        )

    def _set_headerbar_controls(self, *args):
        self.headerbar.set_show_close_button(True)
        self.headerbar.set_title('Infotrans')
