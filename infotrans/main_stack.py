# Copyright 2021
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>

from gi.repository import Gtk
from infotrans.pages.schedule_page import SchedulePage


class MainStack(Gtk.Stack):
    """
    The main stack of the Infotrans Window, contains all the
    pages
    """

    def __init__(self, confman, *args, **kwargs):
        """
        Create a MainStack

        parameters:
            :confman: a infotrans.ConfMan instance
        """
        super().__init__(*args, **kwargs)
        self._confman = confman

        self.set_transition_type(Gtk.StackTransitionType.CROSSFADE)

        self._schedule_stack = SchedulePage(confman=self._confman)
        self.add_titled(self._schedule_stack, 'schedule', 'Schedule')
        self.child_set_property(
            self._schedule_stack,
            'icon-name',
            'alarm-symbolic'
        )

        # We create dummy page because we want the view switcher
        # to exist for testing
        class DummyPage(Gtk.Label):
            def __init__(self, *args, **kwargs):
                super().__init__(*args, **kwargs)
                self.set_label("test")

            def on_appearance(self):
                pass

        self.add_titled(DummyPage(), 'test', 'test')

        self.connect("notify::visible-child", self._on_stack_child_changed)

        self.show_all()

    def _on_stack_child_changed(self, *args):
        # Calling on_appearance starts the lazily loading of the window
        # on most pages.
        self.get_visible_child().on_appearance()
