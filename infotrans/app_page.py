# Copyright 2021
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>

from gi.repository import Gtk, Handy
from infotrans.confman import ConfMan


class InfotransPage(Gtk.Stack):
    """
    A page for Infotrans main window

    The main functionality that this brings is a loading/fetching
    content mechanism
    """

    def __init__(self, confman: ConfMan, *args, **kwargs):
        """
        Create an InfotransPage

        parameters:
            :confman: a infotrans.ConfMan instance

        NOTE: to use this you need to subclass the on_appearance
        and load_content methods
        """
        super().__init__(*args, **kwargs, transition_type=Gtk.StackTransitionType.CROSSFADE,
                         transition_duration=500)
        self._confman = confman

        self._previously_loaded = False

        self._loading_spinner_box = Gtk.Box(orientation="vertical")
        self._loading_spinner = Gtk.Spinner(active=True)
        self._loading_spinner_box.pack_start(
            self._loading_spinner, True, True, False)
        self.add_named(
            self._loading_spinner_box,
            "loading"
        )

    def on_appearance(self):
        """
        Function that gets called when the user switches to the page in
        the stack, or it overwhise apperas.

        The purpose is to load the content while the loading spinner spins,
        and then set it to show up
        """
        # To ensure it doesn't stop spinning, because it does
        if self._previously_loaded == False:
            self._loading_spinner.stop()
            self._loading_spinner.start()
            # Callung load_content will be done by the subclassers

    def load_content(self):
        """
        Load the content of the pagg
        """
        # This makes it hard for the subclass to know if this is the first
        # time if we set it here
        #self._previously_loaded = True

        # The subclassing one will do the actual loading part and
        # setting the visible child
        pass
