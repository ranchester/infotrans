# Copyright 2021
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>

import json
import os

import tamo


class CredentialException(Exception):
    """Exception for TAMO credentials"""
    pass


class ConfMan:
    """
    A configuration management system for Infotrans
    """
    default_schema = {
        "setup_completed": False
    }

    def __init__(self, file_name: str):
        """
        Create a ConfMan instance

        parameters:
            :file_name: - str of what will the file be named, NOTE: this
            is not the full path, only the name
        """
        self._complete_file_location = f"{os.environ['XDG_CONFIG_HOME']}/{file_name}"

        if os.path.isfile(self._complete_file_location):
            self._conf_file_handle = open(self._complete_file_location, "r+")
        else:
            self._conf_file_handle = open(self._complete_file_location, "w+")

        if not (len(self._conf_file_handle.read()) > 0):
            self._conf_file_json_raw = self.default_schema.copy()
        else:
            self._conf_file_handle.seek(0)
            self._conf_file_json_raw = json.load(self._conf_file_handle)

        self._tamo = None

    def _sync(self):
        """
        Sync in memory configuration database to disk
        """
        self._reset_file_handler()
        self._conf_file_handle.seek(0)
        json.dump(self._conf_file_json_raw, self._conf_file_handle)
        self._conf_file_handle.flush()

    def get_value(self, value_name: str):
        """
        Get a value from the configuration management system

        parameters:
            :value_name: - str of the name of the value that you want

        """
        return self._conf_file_json_raw[value_name]

    def set_value(self, value_name: str, payload):
        """
        Set a value in this ConfMan instance

        parameters:
            :value_name: - str of what the value will be named
            :payload: - the payload that you want to store
        """
        self._conf_file_json_raw[value_name] = payload
        self._sync()

    def _get_tamo(self):
        try:
            username = self.get_value("username")
            password = self.get_value("password")
        except:
            raise CredentialException(
                "Failed to retrieve tamo username and password, please put in credentials"
            )
        self._tamo = tamo.Tamo(username, password)

    def _reset_file_handler(self):
        """
        Reset the file handler of the ConfMan
        """
        # Jumping between threads without this messes up the file,
        # so everytime we sync, we call it
        self._conf_file_handle = open(self._complete_file_location, "w+")

    @property
    def tamo(self) -> tamo.Tamo:
        if self._tamo is None:
            self._get_tamo()
            return self._tamo
        return self._tamo
