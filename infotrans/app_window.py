# Copyright 2021
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>

from gi.repository import Gtk, Handy

from infotrans.headerbar import InfotransHeaderbar
from infotrans.main_stack import MainStack


class InfotransWindow(Handy.ApplicationWindow):
    """
    The Infotrans main application window
    """

    def __init__(self, confman, **kwargs):
        """
        Create an Infotrans application window

        parameters:
            :confman: a infotrans.ConfMan instance
        """
        super().__init__(**kwargs)
        self._confman = confman

        self.set_title("Infotrans")
        self.set_icon_name("lt.alm.stalas.Infotrans")
        self.set_default_size(800, 600)

        self._main_box = Gtk.Box(orientation="vertical")
        self._main_stack = MainStack(confman=self._confman)
        self._main_stack.connect(
            'notify::visible-child',
            self._on_main_stack_change
        )
        self._main_stack.set_visible_child_name("schedule")

        self._bottom_bar = Handy.ViewSwitcherBar()
        self._bottom_bar.set_stack(self._main_stack)
        self._headerbar = InfotransHeaderbar(self._main_stack)
        self._on_main_stack_change()
        self._headerbar.connect('headerbar_squeeze',
                                self._on_headerbar_squeeze)

        self._main_box.add(self._headerbar)
        self._headerbar.set_vexpand(False)
        self._main_box.add(self._main_stack)
        self._main_stack.set_vexpand(True)
        self._main_box.add(self._bottom_bar)
        self._bottom_bar.set_vexpand(False)

        self.add(self._main_box)

        # We call the on_appear of the visible child because notify-visible doens't
        # work when first initializing it for the loading screen
        self._main_stack.get_visible_child().on_appearance()

        self.show_all()

    def _on_main_stack_change(self, *args):
        self._headerbar.nobox.set_text(
            self._main_stack.get_visible_child_name()
        )

    def _on_headerbar_squeeze(self, caller, squeezed):
        self._bottom_bar.set_reveal(squeezed)
