# Copyright 2021
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import sys
import threading

from gi.repository import Gtk, Handy

from infotrans.confman import ConfMan


class SetupWindow(Handy.ApplicationWindow):
    """
    The setup window of Infotrans
    """
    __gtype_name__ = 'InfotransSetupWindow'

    def __init__(self, confman: ConfMan, **kwargs):
        """
        Create the setup window

        parameters:
            :confman: - a infotrans.ConfMan, the setup will store the
            colected information with it
        """
        super().__init__(**kwargs)
        self._confman: ConfMan = confman

        # Not class attribute because I don't know how to put class functions
        self.pages = [
            {
                "name": "introduction",
                "ui_file": "setup_introduction_page.ui",
                "top_level": "main_introduction_box",
                "title": "Welcome to Infotrans"
            },
            {
                "name": "credentials",
                "ui_file": "setup_credentials_page.ui",
                "top_level": "main_credentials_box",
                "title": "Authenticate",
                "continuation_build": self._continue_credentials_page_gen,
                "handler": {
                    "on_credential_entry_edited": self._on_credential_entry_edited
                },
                "exit_func": self._on_exiting_credentials_page,
                "enter_func": self._on_enter_credentials_page
            },
            {
                "name": "authentication",
                "ui_file": "setup_auth_progress_page.ui",
                "top_level": "main_box",
                "title": "Authenticating",
                "continuation_build": self._continue_auth_page_gen,
                "enter_func": self._on_enter_auth_page
            }
        ]

        self.set_default_size(400, 245)
        self.set_title("Setup")
        self.set_icon_name("lt.alm.stalas.Infotrans")
        self.set_resizable(False)

        self._main_box = Gtk.Box(orientation="vertical")

        self._hb: Handy.Headerbar = Handy.HeaderBar()
        self._main_box.add(self._hb)

        self._next_button = Gtk.Button.new_with_label("Next")
        self._next_button.set_size_request(80, -1)
        self._next_button.connect("clicked", self._on_next_button_clicked)
        self._next_button.get_style_context().add_class("suggested-action")
        self._hb.pack_end(self._next_button)

        self._cancel_button: Gtk.Button = Gtk.Button.new_with_label("Cancel")
        self._cancel_button.set_size_request(80, -1)
        self._cancel_button.connect("clicked", self._on_cancel_button_clicked)
        self._hb.pack_start(self._cancel_button)

        self._step_stack: Gtk.Stack = Gtk.Stack()
        self._step_stack.set_transition_type(
            Gtk.StackTransitionType.SLIDE_LEFT_RIGHT)
        self._step_stack.set_transition_duration(1000)

        for page in self.pages:
            page_builder = Gtk.Builder.new_from_resource(
                f"/lt/alm/stalas/Infotrans/ui/{page['ui_file']}"
            )

            self._step_stack.add_named(
                page_builder.get_object(page["top_level"]),
                page["name"]
            )

            if "continuation_build" in page:
                page["continuation_build"](page_builder)

            if "handler" in page:
                page_builder.connect_signals(page["handler"])

        self._main_box.pack_end(self._step_stack, True, True, 0)

        self.add(self._main_box)

        self._current_page = 0
        self._update_page()

        self.show_all()

    def _on_cancel_button_clicked(self, button: Gtk.Button):
        sys.exit(0)

    def _on_next_button_clicked(self, button: Gtk.Button):
        self._current_page += 1
        self._update_page()

    def _update_page(self):
        """
        Updates the page of te setup

        When the next button is clicked, we use this to update to the
        next step of the setup process, this is also done at the very
        start of the process.

        It only works if this isn't the last page, and it handlers enter
        and exit functions
        """
        if self._current_page < len(self.pages):
            # Handling of off by one arrows, basically we call the exit function
            # of the previous page when entering a new page.
            if self._current_page != 0 and "exit_func" in self.pages[self._current_page - 1]:
                self.pages[self._current_page - 1]["exit_func"]()

            self._step_stack.set_visible_child_name(
                self.pages[self._current_page]["name"])

            self._hb.props.title = self.pages[self._current_page]["title"]

            if (self._current_page + 1) == len(self.pages):
                self._next_button.set_sensitive(False)

            if "enter_func" in self.pages[self._current_page]:
                self.pages[self._current_page]["enter_func"]()

    def _continue_credentials_page_gen(self, page_builder: Gtk.Builder):
        """
        Continue the generation of the credentials page

        Use is to get the page_builder
        """
        # with page_builder other functions can't really access it, so in continuation we
        # make it a class attribute for the other related functions
        self._credentials_builder: Gtk.Builder = page_builder

    def _on_credential_entry_edited(self, entry: Gtk.Entry):
        """
        Check that happens when a credential entry is edited

        We need this to activate the next and remember password buttons
        only when the user has entered the username and password
        """
        username_entry = self._credentials_builder.get_object("username_entry")
        password_entry = self._credentials_builder.get_object("password_entry")

        # For allowing us to go to the next page and enable remember password only after
        # we entered atleast something into the username and password entries.
        if len(username_entry.get_text()) > 0 and len(password_entry.get_text()) > 0:
            self._credentials_builder.get_object(
                "remember_password_switch").set_sensitive(True)
            self._next_button.set_sensitive(True)

    def _on_exiting_credentials_page(self):
        self._confman.set_value(
            "username", self._credentials_builder.get_object("username_entry").get_text())
        self._confman.set_value(
            "password", self._credentials_builder.get_object("password_entry").get_text())

        self._confman.set_value(
            "remember_password",
            self._credentials_builder.get_object(
                "remember_password_switch").get_active()
        )

    def _on_enter_credentials_page(self):
        """
        Set the next button not sensitive when we enter credentials
        """
        self._next_button.set_sensitive(False)

    def _on_enter_auth_page(self):
        auth_thread = threading.Thread(target=self._attempt_tamo_setup_auth)
        auth_thread.start()

    def _attempt_tamo_setup_auth(self):
        # Seams weird, however that is a property, and it will try logging in
        # with out previously set username and password values
        if self._confman.tamo.logged_in:
            self._confman.set_value("setup_completed", True)
            self._authentication_builder.get_object("auth_spinner").stop()
            self.close()
        else:
            raise Exception("bad credentials, login failed")

    def _continue_auth_page_gen(self, page_builder: Gtk.Builder):
        self._authentication_builder: Gtk.Builder = page_builder
