# Copyright 2021
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>

from gi.repository import Handy, Gtk, GLib
from infotrans.app_page import InfotransPage
from pathlib import Path
import time
import threading
import json
import datetime


@Gtk.Template(resource_path="/lt/alm/stalas/Infotrans/ui/lesson_row.ui")
class InfotransLessonRow(Gtk.ListBoxRow):
    """
    A row for a single infotrans lesson
    """

    __gtype_name__ = "InfotransLessonRow"

    _teacher_name_label = Gtk.Template.Child()
    _lesson_time_label = Gtk.Template.Child()
    _lesson_name_label = Gtk.Template.Child()

    def __init__(self, start_time, lesson_name, teacher_name, *args, **kwargs):
        """
        Create a InfotransLessonRow

        parameters:
           :start_time: string representation of when the lesson starts in %h:%m
           :lesson_name: full lesson name
           :teacher_name: full name of the teacher that will be teaching the lesson
        """
        super().__init__(*args, **kwargs)
        self._lesson_time_label.set_label(start_time)
        self._lesson_name_label.set_label(lesson_name)
        self._teacher_name_label.set_label(teacher_name)

        self.show_all()

    def setup_sizegroup(self, sizegroup: Gtk.SizeGroup):
        sizegroup.add_widget(self._teacher_name_label)


class TodayLabel(Gtk.Label):
    """
    A label for indicating which day is today

    This is however basically just a label with a css style class that
    only shows "Now"
    """

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.set_label("Now")
        self.get_style_context().add_class("badge")


class SchedulePage(InfotransPage):
    """
    The Infotrans schedule page

    This is the actual thing that becomes a child of the view switcher
    """

    __gtype_name__ = "InfotransSchedulePage"

    # Not gresource because I cant figure out how
    # to open a gresource as a standard file
    # in python
    _day_map = [
        "Monday",
        "Tueday",
        "Wednesday",
        "Thursday",
        "Friday",
        "Saturday",
        "Sunday"
    ]

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._schedule_builder = Gtk.Builder.new_from_resource(
            "/lt/alm/stalas/Infotrans/ui/schedule_page.ui"
        )

        self._content = self._schedule_builder.get_object(
            "schedule_scrolled_win"
        )
        self.add_named(
            self._content,
            "content_window"
        )

        self.show_all()

    def on_appearance(self):
        super().on_appearance()
        self.load_content()

    def load_content(self):
        super().load_content()
        if self._previously_loaded == True:
            self.set_visible_child_name("content_window")
        else:
            thread = threading.Thread(
                target=self._async_load_tamo_content_target)
            thread.daemon = True
            thread.start()

    def _blocking_tamo_schedule_io(self):
        # TAMO will now fetch the schedule and cache everything
        self._confman.tamo.schedule

    def _async_load_tamo_content_target(self):
        """
        Dummy tamo loading target that calls the blocking io concurently
        in the method that was stated in the PyGObject documentation
        """
        self._blocking_tamo_schedule_io()
        GLib.idle_add(self._async_load_tamo_content)

    def _async_load_tamo_content(self):
        # This isn't blocking because we already downloaded the schedule
        # in _blocking_tamo_schedule_io, and TAMO caches all of that

        week_listbox = self._schedule_builder.get_object("week_listbox")

        teacher_label_sizegroup = Gtk.SizeGroup(
            mode=Gtk.SizeGroupMode.HORIZONTAL)

        # Since now we are using a listbox in a listbox instead of just a listbox,
        # we have to make the toplevel listbox have transparent background
        week_listbox.get_style_context().add_class("transparent")
        for d_idx, day in enumerate(self._confman.tamo.schedule):

            # We need this row instead of just adding tmp_day_listbox
            # directly into week_listbox because we need to disable
            # the activatable and selectable property, which I don't know how to do
            # on a Gtk.ListBox, not row
            tmp_day_week_listbox_row = Gtk.ListBoxRow(
                activatable=False,
                can_focus=False
            )
            tmp_day_listbox = Gtk.ListBox(
                selection_mode=Gtk.SelectionMode.NONE,
                margin_top=20,
                margin_bottom=20
            )
            tmp_day_listbox.get_style_context().add_class("content")

            tmp_row = Handy.ExpanderRow(
                title=self._day_map[d_idx],
                enable_expansion=True
            )

            if datetime.datetime.today().weekday() == d_idx:
                tmp_row.add_action(TodayLabel())
                tmp_row.set_expanded(True)

            lesson_listbox = Gtk.ListBox(selection_mode=Gtk.SelectionMode.NONE)
            for l_idx, lesson in enumerate(day):
                tmp_lesson_row = InfotransLessonRow(
                    start_time=lesson.start.strftime("%H:%M"),
                    lesson_name=lesson.name,
                    teacher_name=lesson.teacher.name,
                    activatable=False
                )
                tmp_lesson_row.setup_sizegroup(teacher_label_sizegroup)

                # We get weird double border if we add on the first one
                if l_idx != 0:
                    tmp_separator_row = Gtk.ListBoxRow(
                        can_focus=False, activatable=False)
                    tmp_separator_row.add(Gtk.Separator(can_focus=False))
                    lesson_listbox.add(tmp_separator_row)
                lesson_listbox.add(tmp_lesson_row)

            if not day.empty:
                tmp_row.add(lesson_listbox)
            else:
                no_lessons_label_builder = Gtk.Builder.new_from_resource(
                    "/lt/alm/stalas/Infotrans/ui/no_lessons_label.ui"
                )
                tmp_row.add(no_lessons_label_builder.get_object(
                    "no_lessons_label"))

            tmp_day_listbox.add(tmp_row)
            tmp_day_week_listbox_row.add(tmp_day_listbox)
            week_listbox.add(tmp_day_week_listbox_row)

        self.show_all()
        self.set_visible_child_name("content_window")
        self._previously_loaded = True
